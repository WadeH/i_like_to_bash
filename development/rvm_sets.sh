#!/bin/bash
# RVM Gemset Info
function all_sets {
	local gs=$(ls ~/.rvm/wrappers | wc -l)
	printf "\033[0;32mAvailable GemSets:\033[0;37m\n"
	for g in $( $gs ); do
		printf "\t* $g\n"
	done
}

all_sets