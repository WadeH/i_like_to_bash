# !/bin/bash
# Misc launcher (MISSILE)
# @author vdtdev@gmail.com
function banner {
	printf "\033[1;31mMISSILE (Misc) \033[1;37mMisc Launcher\n\033[0;37m"
}

function colored {
	local cs="\033[b;cm"
	cs=${cs/b/$color_bright}
	cs=${cs/c/$color_fg}
	local csb="xy\033[0;32m"
	csb=${csb/y/$color_text}
	csb=${csb/x/$cs}
	colored_text=${csb}
}

function banner_usage {
	printf "\nUsage: missile <\033[1;32maction\033[0;37m> [\033[1;33margs...\033[0;37m]\n\n"
	printf "\033[1;37m\tAction\tArgs\t\t\tDescription\n\033[0;37m"
	printf "\t\033[1;32m--help\t\033[0;37m--\t\t\tThis help message\n"
	printf "\t\033[1;32m--dj\t\033[0;37m[\033[1;33mstart\033[0;37m|\033[1;33mstop\033[0;37m|\033[1;33mrestart\033[0;37m]\tDelayed Job\n"
}

function delayed_job_action {
	bundle exec script/delayed_job $1
}

function missile_main {
	banner
	if [ $# -lt 1 ]; then
		banner_usage
		exit
	fi
	if [ $1 = "--help" ]; then
		banner_usage
		exit
	fi
	if [ $1 = "--dj" ]; then
		delayed_job_action $2
		exit
	fi
}

missile_main $*