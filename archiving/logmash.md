LogMash
=======

## Overview

LogMash is a shell script for compressing one or more files matching a regular expression.

Matched files are compressed using Tar/BZip2, and if successful, the original uncompressed files are removed.


## Usage

``` sh logmash.sh [filter] ```

- `[filter]` Regular expression matching one or more files in the current directory

## Example

__Input__

```sh logmash.sh log\.[0-9]+\.log```

__Output__

	Found 2 log files to attempt to compress
	log.20150401.log ==> log.20150401.log.bz2
	a log.20150401.log
		+ Successful Removing log.20150401.log
	log.20150402.log ==> log.20150402.log.bz2
	a log.20150402.log
		+ Successful Removing log.20150402.log



