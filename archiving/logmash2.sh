#!/bin/bash
# .Log -> Bz2 Compression Script
function compress_logs {
  printf "Batch Compression Script\n\n"
  if [ $# -ne 1 ]; then
    printf "Not enough parameters\n"
    compress_usage
    exit
  fi
  if [ $1 = "--help" ]; then
    compress_usage
    exit
  fi
  local fc=$(ls | egrep $1 | wc -l)
  echo Found $fc log files to attempt to compress
  for i in $( ls | egrep $1 ); do
    if [ -e ${i}.bz2 ];
    then
      printf "\t* $i \033[1;31malready \033[0;37marchived.\n"
    else
      printf "$i ==> ${i}.bz2\n"
      tar cvjf ${i}.bz2 $i
      local raws=$(stat -f "%z" $i)
      local bz2=$(stat -f "%z" $i.bz2)
      # Check if we just made a bigger file T_T
      if [ -e ${i}.bz2 ];
      then
        if [[ $bz2 > $raws ]];
        then
          printf "\t* \033[1;33m$bz2 > $raws \033[0;37mRemoving $i.bz2\n"
          rm $i.bz2
        else
          rm $i
          printf "\t+ \033[1;32mSuccessful \033[0;37mRemoving $i\n"
        fi
      else
        printf "\t- 033[1;31mUnsuccessful \033[0;37$i Not removed\n"
      fi
    fi
  done
}

function compress_usage {
  printf "Usage: logmash [filter]\n\t[filter]\t Regular expression to grep for target files\n"
}
compress_logs $1
