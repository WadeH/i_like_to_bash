#!/bin/bash
# .Log -> Bz2 Compression Script
function compress_logs {
  printf "Batch Compression Script\n\n"
  if [ $# -ne 1 ]; then
    printf "Not enough parameters\n"
    compress_usage
    exit
  fi
  if [ $1 = "--help" ]; then
    compress_usage
    exit
  fi
  local fc=$(ls | egrep $1 | wc -l)
  echo Found $fc log files to attempt to compress
  for i in $( ls | egrep $1 ); do
    if [ -e ${i}.bz2 ];
    then
      printf "\t* $i \033[1;31malready \033[0;37marchived.\n"
    else
      local raws=$(stat -f "%z" $i)
      local bz2=$(stat -f "%z" $i.bz2)
      printf "$i ==> ${i}.bz2\n"
      tar cvjf ${i}.bz2 $i
      if [ -e ${i}.bz2 ];
      then
        rm $i
        printf "\t+ \033[1;32mSuccessful \033[0;37mRemoving $i\n"
      else
        printf "\t- 033[1;31mUnsuccessful \033[0;37$i Not removed\n"
      fi
    fi
  done
}

function compress_usage {
  printf "Usage: logmash [filter]\n\t[filter]\t Regular expression to grep for target files\n"
}
compress_logs $1
