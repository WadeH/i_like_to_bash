COLOR_NAMES=["BLACK", "RED", "GREEN", "YELLOW", "BLUE", "PURPLE", "CYAN", "GRAY"]
COLOR_CODES=30..37
def color_code(cname,v):
	code=0
	for c in COLOR_NAMES:
		if c==cname:
			code=COLOR_CODES[COLOR_NAMES.index(c)]
	return str(v) + ";" + str(code) + "m"


if __name__ == '__main__':
	print color_code(sys.argv[0], sys.argv[1])
	return 0

