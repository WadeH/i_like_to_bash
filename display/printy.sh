#!/bin/bash
# Function that echos text using the provided color code
# 
# Example:
# 	printy "0;31m" "Red Text"
#
# vdtdev@gmail.com
# v2015.04.09
function printy
{
	local prefix="\033["
	local text=$2
	local color=$1
	local colortext="$prefix $color $text"

	echo -en $colortext

}

function pcolor
{
	echo python pcolor.py $1 $2
	read cv
	return $cv
}